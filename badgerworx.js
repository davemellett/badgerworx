window.addEventListener("load", function () {
  const password = document.querySelector("#password");
  const username = document.querySelector("#username");
  const login_button = document.querySelector("#login_button");

  // disable login button until username and password have been entered
  login_button.disabled = true;

  const validate_fields = () => {
    if (password.value.length > 0 && username.value.length > 0) {
      login_button.disabled = false;
    } else {
      login_button.disabled = true;
    }
  };

  username.addEventListener("input", (event) => {
    validate_fields();
  });

  password.addEventListener("input", (event) => {
    validate_fields();
  });
});
