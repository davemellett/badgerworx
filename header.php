<header>
    <div class="left side">
        <a href="index.php"><img src="badgerworx.jpg" alt="Badgerworx logo" /></a>
    </div>
    <div class="middle"><h1>Badgerworx</h1></div>
    <div class="right side">
        <?php
            if ($_SESSION['loggedin']) { 
                echo htmlspecialchars($_SESSION['username']);
            } else {
                if (!$logging_in){
                    echo '<form action="login_page.php" method="post">
                    <button type="submit">Login</button>
                    </form>';
                }
            }
        ?>
    <div>
</header>