<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home Page</title>
    <link rel="stylesheet" href="badgerworx.css" />
  </head>
  <body>
    <?php include('header.php'); ?>
    <div class="wrapper">
      <div class="container home" role="main">
        <img src="badgerworx.jpg" alt="Badgerworx logo" />
        <?php 
          if ('failed' === $_GET['login']) {
            echo '<div
              class="auth_error" 
              role="alert" 
              aria-live="assertive" 
            >Invalid credentials</div>';
            echo '<div>Please <a href="login_page.php" aria-label="Login to Badgerworx">try again</a>.</div>';

          } else {
            echo '<div>Please <a href="login_page.php" aria-label="Login to Badgerworx">login</a> to continue.</div>';
          }
        ?>
      </div>
    </div>
    <noscript>
      <div class="wrapper">
        <div class="noscript">
          <div class="container home" role="main">
            Scripting is currently turned off or unavailable in the browser. 
            Some features may not be working but you will still be able to use the site.
          </div>
        </div>
      </div>
    </noscript>
  </body>
</html>
