<?php
session_start();

$users = [
    'admin' => 'password123',
    'roo' => 'password'
];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    if (isset($users[$username]) && $users[$username] == $password) {
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $username;
        header('Location: secured.php');
        exit();
    } 
}

header('Location: index.php?login=failed');
exit();
?>
