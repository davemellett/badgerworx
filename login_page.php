<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login Page</title>
    <link rel="stylesheet" href="badgerworx.css" />
  </head>

  <body>
    <?php 
      $logging_in = true;
      include('header.php'); 
    ?>
    <div class="wrapper">
        <div class="container" role="main">
        <h2>Login</h2>
        <form action="login.php" method="post">
            <div class="form-group">
              <label for="user">Username:</label>
              <input
                  type="text"
                  id="username"
                  name="username"
                  required
                  aria-required="true"
              />
            </div>
            <div class="form-group">
              <label for="pass">Password:</label>
              <input
                  type="password"
                  id="password"
                  name="password"
                  required
                  aria-required="true"
              />
            </div>
            <button id="login_button" type="submit">Login</button>
        </form>
        </div>
    </div>
    <script defer src="badgerworx.js"></script>

  </body>
</html>
