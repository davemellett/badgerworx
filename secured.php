<?php
session_start();

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Basic realm="Login Required"');
    echo 'Unauthorized';
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Secured Page</title>
    <link rel="stylesheet" href="badgerworx.css" />

</head>
<body>
<?php include('header.php'); ?>
    <div class="wrapper">
        <div class="container home" role="main">
            <img src="badgerworx.jpg" alt="Badgerworx logo" />
            <h2>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?>!</h2>
            <form action="logout.php" method="post">
                <button type="submit">Logout</button>
            </form>
        </div>
    </div>
</body>
</html>
